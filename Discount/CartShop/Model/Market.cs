﻿using ProductService;
using System.Collections.Generic;
using static ProductService.ProductService;

namespace CartShop.Model
{
    public static class Market
    {
        public static IEnumerable<Product> markets = new List<Product>()
        {
            // Beverage
            GetCoke(),
            GetLactasoyMilk225ML(),
            GetLactasoyMilk450ML(),
            GetMeijiMilk225ML(),
            GetMeijiMilk450ML(),
            GetSparklingWater(),

            // Bread
            GetCheeseRiceBall(),
            GetBread97(),
            GetBread44(),
            GetTunaRiceBall()
        };

        public static Dictionary<int, Product> dictProduct = new Dictionary<int, Product>()
        {
            // Beverage
            { 1, GetCoke() },
            { 2, GetLactasoyMilk225ML() },
            { 3, GetLactasoyMilk450ML() },
            { 4, GetMeijiMilk225ML() },
            { 5, GetMeijiMilk450ML() },
            { 6, GetSparklingWater() },

            // Bread
            { 7, GetCheeseRiceBall() },
            { 8, GetBread97() },
            { 9, GetBread44() },
            { 10, GetTunaRiceBall() }
        };
    }
}