﻿using System;
using System.Collections.Generic;
using System.Linq;

internal static class ProgramHelpers
{
    private static void ConcepGamma() // Rule 1
    {
        List<string> itemsList = new List<string> { "Cheese Sandwich", "Tuna Sandwich", "Bread awod", "Lactwasoy Mwilk 225ML", "CowkeCola" };
        List<string> beverages = new List<string> { "CokeCola", "Lactasoy", "Milk", "Water" };

        int beverageCount = itemsList.Count(item => beverages.Any(beverage => item.Contains(beverage)));
        bool hasOneOrTwoBeverages = beverageCount == 1 || beverageCount == 2;

        Console.WriteLine($"Does the list contain 1 or 2 beverages? {hasOneOrTwoBeverages}");

        //var sumOfAllPrice = beverageProducts.Select(x => x.Price * x.Quantity).Sum();
        //var sumOfAllberage = beverageProducts.Sum(item => item.Quantity);

        //var groupedProducts = beverageProducts
        //    .GroupBy(p => _keywordList.FirstOrDefault(keyword => p.Name.Contains(keyword)))
        //    .Select(g => new
        //    {
        //        Keyword = g.Key,
        //        TotalQuantity = g.Sum(p => p.Quantity),
        //    });
    }

    // 150 + 150 = 300 * 0.5 = 150
    // 5 EA of 97
    // 5 EA of 44
    // 2 pair of 44
    // 2 pair of 97
    // 1 pair of 44 and 97

    //5 ea of 97
    //2 ea of 44

    // 97 with 44 => 2
    // 97 with 97 => 2
    //97

    // Real answer
    // 2 pair of 97 (4 EA)
    // left one of 97 (1 EA)
    // 1 pair of 44 (2 EA)
    private static void ConceptAlpha()
    {
        List<string> sandwiches = new List<string> { "Cheese Sandwich", "Tuna Sandwich" };

        bool containsSandwich = sandwiches.Any(s => s.Contains("Sandwich"));

        Console.WriteLine($"Does the list contain 'Sandwich'? {containsSandwich}");
    }

    private static void ConceptBeta()
    {
        List<string> sandwiches2 = new List<string> { "Cheese Sandwich", "Tuna Sandwich", "Bred awod" };
        List<string> searchTerms = new List<string> { "Bread", "Sandwich" };

        bool allMatch = searchTerms.All(term => sandwiches2.Any(item => item.Contains(term)));

        Console.WriteLine($"Do all search terms match items in the list? {allMatch}");
    }

    private static void ConceptDelta() // Rule 3
    {
        List<string> itemsList = new List<string> { "Sandwich", "Meiji", "Sandwich", "Meiji", "Bread 97" };
        List<string> keywords = new List<string> { "Sandwich", "Meiji" };

        var keywordCounts = keywords.ToDictionary(
                keyword => keyword,
                keyword => itemsList.Count(item => item.Contains(keyword))
            );

        // Determine the maximum number of complete pairs that can be formed
        int maxPairs = keywordCounts.Values.Min();

        // Create a list to hold the pairs
        List<List<string>> pairs = new List<List<string>>();
        for (int i = 0;i < maxPairs;i++)
        {
            pairs.Add(new List<string>(keywords));
        }

        Console.WriteLine("Pairs found:");
        foreach (var pair in pairs)
        {
            Console.WriteLine($"{{ {string.Join(", ", pair)} }}");
        }
    }

    private static void ConceptGamma()
    {
        //var fiveCoke = GetProductInPack(new[] { new Cart(GetCoke(), 8) });
        //var threeLactasoy225 = GetProductInPack(new[] { new Cart(GetLactasoyMilk225ML(), 4) });
        //var threeMeiji225 = GetProductInPack(new[] { new Cart(GetMeijiMilk225ML(), 4) });
        //var threeMeiji450 = GetProductInPack(new[] { new Cart(GetMeijiMilk450ML(), 4) });
        //var threeLactasoy450 = GetProductInPack(new[] { new Cart(GetLactasoyMilk450ML(), 3) });
        //var threeCheeseRiceBall = GetProductInPack(new[] { new Cart(GetCheeseRiceBall(), 4) });
        //var threeTunaRiceBall = GetProductInPack(new[] { new Cart(GetTunaRiceBall(), 4) });
        //var fiveBread97 = GetProductInPack(new[] { new Cart(GetBread97(), 6) });
        //var fiveBread44 = GetProductInPack(new[] { new Cart(GetBread44(), 3) });

        //var invoice = new Invoice(rules, new ShoppingCart());

        //invoice.Cart.Add(fiveCoke);
        //invoice.Cart.Add(threeLactasoy225);
        //invoice.Cart.Add(threeLactasoy450);
        //invoice.Cart.Add(threeCheeseRiceBall);
        //invoice.Cart.Add(threeTunaRiceBall);
        //invoice.Cart.Add(fiveBread44);
        //invoice.Cart.Add(fiveBread97);
        //invoice.Cart.Add(threeMeiji225);
        //invoice.Cart.Add(threeMeiji450);

        //PrintInvoice(invoice);
    }
}