﻿using CartShop.Control;
using CartShop.Factory;
using CartShop.View;
using System;

namespace CartShop
{
    internal class Program
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        private static void Main(string[] args)
        {
            var platform = new Platform(new ConsoleDisplay(),
                                        CommandFactory.CreateCommand(RuleFactory.rules));
            platform.Run();

            Console.WriteLine("The program has stopped running.");
            Console.ReadKey();
        }
    }
}