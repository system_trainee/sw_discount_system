﻿namespace CartShop.View
{
    public interface IDisplay
    {
        string GetInput();

        void Print(string message);

        void PrintLine(string message);

        void PrintLine(string format, params object[] arg);

        void ReadKey();

        void Clear();
    }
}