﻿using System;

namespace CartShop.View
{
    public class ConsoleDisplay: IDisplay
    {
        public virtual void PrintLine(string message)
        {
            Console.WriteLine(message);
        }

        public virtual void Print(string message)
        {
            Console.Write(message);
        }

        public virtual string GetInput()
        {
            return Console.ReadLine().Trim();
        }

        public void PrintLine(string format, params object[] arg)
        {
            Console.WriteLine(format, arg);
        }

        public void ReadKey()
        {
            Console.ReadKey();
        }

        public void Clear()
        {
            Console.Clear();
        }
    }
}