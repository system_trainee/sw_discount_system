﻿using CartShop.Control;
using CartShop.Control.Command;
using CartShop.View;
using DiscountLibrary.Model;
using System.Collections.Generic;
using System.Threading;

namespace CartShop.Factory
{
    internal class CommandFactory
    {
        public static CommandControl CreateCommand(List<ISaleRule> rules)
        {
            return new CommandControl(new List<IPlatformCommand>(),
                                                                new CancellationTokenSource(),
                                                                new Invoice(rules, new ShoppingCart()),
                                                                new ConsoleDisplay());
        }
    }
}