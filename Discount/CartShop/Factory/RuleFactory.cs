﻿using DiscountLibrary.Key;
using DiscountLibrary.Model;
using System.Collections.Generic;

namespace CartShop.Factory
{
    internal static class RuleFactory
    {
        internal static List<ISaleRule> rules = new List<ISaleRule>(){
            new SaleRule(new Beverage(new BeverageKeywords())),

            new SaleRule(new Bread(new BreadKeywords())),

            new SaleRule(new Promotion(new PromotionRiceBallKeywords(),
                                       new PromotionMeijiKeywords(),
                                       GetPromotionDiscountFactor())),

            new SaleRule(new Rebate(5000,500))
        };

        internal static decimal GetBreadDiscountFactor()
        {
            return 0.5m;
        }

        internal static Dictionary<(decimal SandwichPrice, decimal MilkPrice), decimal> GetPromotionDiscountFactor()
        {
            return new Dictionary<(decimal SandwichPrice, decimal MilkPrice), decimal>
            {
                {(200m, 200m), 300m},  // Sandwich $200 + Milk $200 costs $300
                {(200m, 300m), 400m},  // Sandwich $200 + Milk $300 costs $400
                {(300m, 300m), 400m}   // Sandwich $300 + Milk $300 costs $400
            };
        }
    }
}