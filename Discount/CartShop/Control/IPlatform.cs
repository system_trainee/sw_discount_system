﻿namespace CartShop.Control
{
    public interface IPlatform
    {
        void Run();
    }
}