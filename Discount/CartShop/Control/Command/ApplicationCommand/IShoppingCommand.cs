﻿namespace CartShop.Control.Command.ApplicationCommand
{
    public interface IShoppingCommand
    {
        ShopState Id { get; }

        string Name { get; }

        void Execute();
    }
}