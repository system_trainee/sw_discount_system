﻿using CartShop.Control.Command.ApplicationCommand.ShopProcess;
using CartShop.View;
using DiscountLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CartShop.Control.Command.ApplicationCommand
{
    public class Shop: PlatformCommand, IEquatable<Shop>
    {
        public override CommandId Id => CommandId.Shop;
        private readonly Invoice _invoice;
        private readonly IDisplay _display;
        private readonly List<IShoppingCommand> commandList;
        private CancellationTokenSource _token;

        public Shop()
        {
            commandList = new List<IShoppingCommand>();
            _token = new CancellationTokenSource();
        }

        public Shop(Invoice invoice, IDisplay display)
            : this()
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _invoice = invoice ?? throw new ArgumentNullException(nameof(invoice));
            RegisterCommand();
        }

        public new string Name => this.GetType().Name;

        public override void Execute()
        {
            RenewCancellationToken();

            while (!_token.IsCancellationRequested)
            {
                _display.Clear();
                _display.PrintLine("Current Items: {0} | Total Quantity: {1}",
                                    _invoice.Cart.UserCart.Count(), _invoice.Cart.UserCart.Sum(p => p.Quantity));
                _display.PrintLine("\nAvailable Commands:\n" +
                    "1. Add to Cart\n" +
                    "2. Remove item from Cart\n" +
                    "3. Edit Quantity\n" +
                    "4. View Cart\n" +
                    "5. Make Purchase\n" +
                    "6. Back");

                _display.Print("\n> ");
                string input = _display.GetInput();
                if (string.IsNullOrWhiteSpace(input))
                    continue;

                var command = ParseData(input);

                if (command != ShopState.Invalid)
                    ExecuteCommand(command);
                else
                {
                    _display.PrintLine("\nInvalid Commnad");
                    _display.ReadKey();
                }
            }
        }

        private void ViewCart()
        {
            _display.PrintLine("Current cart items:\n");
            _display.PrintLine("=========================================================================================");
            _display.PrintLine("{0,-3} | {1,-30} | {2,-15} | {3,-10} | {4,-15}", "No.", "Product", "Unit Price", "Quantity", "Total");
            _display.PrintLine("=========================================================================================");

            int index = 1;
            foreach (var item in _invoice.Cart.UserCart)
            {
                _display.PrintLine("{0,-3} | {1,-30} | {2,-15:C} | {3,-10} | {4,-15:C}",
                    index++, item.Name, item.Price, item.Quantity, item.Quantity * item.Price);
            }
            _display.PrintLine("=========================================================================================");
        }

        private void RegisterCommand()
        {
            commandList.Add(new Add(_display, _invoice));
            commandList.Add(new Remove(_display, ViewCart, _invoice));
            commandList.Add(new Edit(_display, ViewCart, _invoice));
            commandList.Add(new ViewCart(_display, _invoice));
            commandList.Add(new MakePurchase(_display, _invoice));
            commandList.Add(new Back(_display, _token));
            commandList.Add(new Test(_display, _invoice));
        }

        private ShopState ParseData(string input)
        {
            if (Enum.TryParse(input, out ShopState command))
                return command;
            return ShopState.Invalid;
        }

        private void ExecuteCommand(ShopState commandId)
        {
            var command = commandList.FirstOrDefault(c => c.Id == commandId);
            if (command != null)
            {
                _display.PrintLine(command.Name);
                command.Execute();
            }
        }

        private void RenewCancellationToken()
        {
            _token?.Cancel();
            _token = new CancellationTokenSource();
            commandList.Clear();
            RegisterCommand();
        }

        public override bool Equals(object obj)
        {
            return obj is Shop otherCommand && Equals(otherCommand);
        }

        public bool Equals(Shop other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}