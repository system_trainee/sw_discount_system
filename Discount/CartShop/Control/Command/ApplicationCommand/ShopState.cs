﻿namespace CartShop.Control.Command.ApplicationCommand
{
    public enum ShopState
    {
        Add = 1,
        Remove = 2,
        Edit = 3,
        ViewCart = 4,
        MakePurchase = 5,
        Back = 6,
        Invalid = 7,
        Test = 99,
    }
}