﻿using CartShop.View;
using DiscountLibrary.Model;
using System;
using System.Linq;

namespace CartShop.Control.Command.ApplicationCommand.ShopProcess
{
    public class Edit: ShoppingCommand, IEquatable<Edit>
    {
        public override ShopState Id => ShopState.Edit;
        private readonly IDisplay _display;
        private readonly Action _viewCart;
        private readonly Invoice _invoice;

        public Edit(IDisplay display, Action viewCart, Invoice invoice)
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _viewCart = viewCart ?? throw new ArgumentNullException(nameof(viewCart));
            _invoice = invoice ?? throw new ArgumentNullException(nameof(invoice));
        }

        public override void Execute()
        {
            if (!_invoice.Cart.UserCart.Any())
            {
                _display.PrintLine("You don't have item in Cart!!\n");
                _display.ReadKey();
                return;
            }

            _viewCart();
            _display.PrintLine("Enter the number of the product to edit:");
            _display.Print("> ");
            if (int.TryParse(_display.GetInput(), out int productIndex)
                && productIndex > 0
                && productIndex <= _invoice.Cart.UserCart.Count())
            {
                var itemToEdit = _invoice.Cart.UserCart.ElementAt(productIndex - 1);
                _display.PrintLine("Enter the new quantity:");

                if (int.TryParse(_display.GetInput(), out int newQuantity) && newQuantity > 0)
                    _invoice.Cart.Edit(itemToEdit, newQuantity);
                else
                    _display.PrintLine("Invalid quantity. Please try again.");
            }
            else
            {
                _display.PrintLine("Invalid selection. Please try again.");
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Edit otherCommand && Equals(otherCommand);
        }

        public bool Equals(Edit other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}