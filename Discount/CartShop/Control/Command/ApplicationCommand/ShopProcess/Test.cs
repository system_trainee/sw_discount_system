﻿using CartShop.View;
using DiscountLibrary.Model;
using System;
using System.Linq;

namespace CartShop.Control.Command.ApplicationCommand.ShopProcess
{
    public class Test: ShoppingCommand, IEquatable<Test>
    {
        public override ShopState Id => ShopState.Test;
        private readonly IDisplay _display;
        private readonly Invoice _invoice;

        public Test(IDisplay display, Invoice invoice)
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _invoice = invoice ?? throw new ArgumentNullException(nameof(invoice));
        }

        public override void Execute()
        {
            var pickedup = ProductService.ProductService.GetProductInPack(new[] {
                new ProductService.ProductService.Cart (ProductService.ProductService.GetCoke(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetSparklingWater(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetBread44(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetBread97(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetTunaRiceBall(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetCheeseRiceBall(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetMeijiMilk225ML(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetMeijiMilk450ML(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetLactasoyMilk225ML(), 5),
                new ProductService.ProductService.Cart (ProductService.ProductService.GetLactasoyMilk450ML(), 5),
            });

            _invoice.Cart.Add(pickedup);
            PrintInvoice();
            _display.PrintLine("Press any key to back to shop menu");
            _display.ReadKey();
        }

        private void PrintInvoice()
        {
            _display.Clear();
            _display.PrintLine("Invoice Summary:\n");
            _display.PrintLine("==========================================================================");
            _display.PrintLine("{0,-35} | {1,-10} | {2,-8} | {3,-8} ", "Product", "Unit Price", "Quantity", "Total");
            _display.PrintLine("==========================================================================");

            foreach (var item in _invoice.Cart.UserCart)
                _display.PrintLine("{0,-35} | {1,10:C} | {2,-8} | {3,-17:C} ",
                    item.Name, item.Price, item.Quantity, item.Quantity * item.Price);

            _display.PrintLine("__________________________________________________________________________");
            _display.PrintLine("{0,-45} {1,26:C} ", "Total Amount Due:", _invoice.TotalAmount);
            _display.PrintLine("__________________________________________________________________________");

            decimal discount = _invoice.TotalDiscount;
            foreach (var saleRule in _invoice.GetSaleRuleInformation()
                                             .Where(rule => rule != null && rule.DiscountAmount > 0))
            {
                if (saleRule.Description.Any())
                    _display.PrintLine("Discount for {0,-38} ", saleRule.Name);

                foreach (var description in saleRule.Description)
                    _display.PrintLine("  {0,-45} -{1,8:C}",
                        $"{description.Quantity} sets of {description.Name}", description.Amount);

                _display.PrintLine("Total for {0,-35} {1,26:C}", saleRule.Name, saleRule.DiscountAmount);
                _display.Print("\n");
            }
            _display.PrintLine("__________________________________________________________________________");
            _display.PrintLine("{0,-45} {1,26:C} ", "Total Discount Applied:", discount);
            _display.PrintLine("{0,-45} {1,26:C} ", "Net Price:", _invoice.TotalAmount - discount);
            _display.PrintLine("==========================================================================");
            _invoice.Cart.Clear();
        }

        public override bool Equals(object obj)
        {
            return obj is Test otherCommand && Equals(otherCommand);
        }

        public bool Equals(Test other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}