﻿using CartShop.View;
using DiscountLibrary.Model;
using System;
using System.Linq;

namespace CartShop.Control.Command.ApplicationCommand.ShopProcess
{
    public class ViewCart: ShoppingCommand, IEquatable<ViewCart>
    {
        public override ShopState Id => ShopState.ViewCart;
        private readonly IDisplay _display;
        private readonly Invoice _invoice;

        public ViewCart(IDisplay display, Invoice invoice)
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _invoice = invoice ?? throw new ArgumentNullException(nameof(invoice));
        }

        public override void Execute()
        {
            if (!_invoice.Cart.UserCart.Any())
            {
                _display.PrintLine("You don't have item in Cart!!\n");
                _display.ReadKey();
                return;
            }

            _display.PrintLine("Current cart items:\n");
            _display.PrintLine("=========================================================================================");
            _display.PrintLine("{0,-3} | {1,-30} | {2,-15} | {3,-10} | {4,-15}", "No.", "Product", "Unit Price", "Quantity", "Total");
            _display.PrintLine("=========================================================================================");

            int index = 1;
            foreach (var item in _invoice.Cart.UserCart)
            {
                _display.PrintLine("{0,-3} | {1,-30} | {2,-15:C} | {3,-10} | {4,-15:C}",
                    index, item.Name, item.Price, item.Quantity, item.Quantity * item.Price);
                index++;
            }
            _display.PrintLine("=========================================================================================");
            _display.PrintLine("Press any key to back to shop menu");
            _display.ReadKey();
        }

        public override bool Equals(object obj)
        {
            return obj is ViewCart otherCommand && Equals(otherCommand);
        }

        public bool Equals(ViewCart other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}