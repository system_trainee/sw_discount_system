﻿using CartShop.View;
using DiscountLibrary.Model;
using System;
using System.Linq;

namespace CartShop.Control.Command.ApplicationCommand.ShopProcess
{
    public class Remove: ShoppingCommand, IEquatable<Remove>
    {
        public override ShopState Id => ShopState.Remove;
        private readonly IDisplay _display;
        private readonly Action _viewCart;
        private readonly Invoice _invoice;

        public Remove(IDisplay display, Action viewCart, Invoice invoice)
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _viewCart = viewCart ?? throw new ArgumentNullException(nameof(viewCart));
            _invoice = invoice ?? throw new ArgumentNullException(nameof(invoice));
        }

        public override void Execute()
        {
            if (!_invoice.Cart.UserCart.Any())
            {
                _display.PrintLine("You don't have item in Cart!!\n");
                _display.ReadKey();
                return;
            }

            _viewCart();
            _display.PrintLine("Enter the number of the product to remove:");
            _display.Print("> ");
            if (int.TryParse(_display.GetInput(), out int productIndex)
                && productIndex > 0
                && productIndex <= _invoice.Cart.UserCart.Count())
            {
                var itemToRemove = _invoice.Cart.UserCart.ElementAt(productIndex - 1);
                _invoice.Cart.Remove(itemToRemove);
            }
            else
            {
                _display.PrintLine("Invalid selection. Please try again.");
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Remove otherCommand && Equals(otherCommand);
        }

        public bool Equals(Remove other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}