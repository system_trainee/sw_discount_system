﻿using CartShop.Model;
using CartShop.View;
using DiscountLibrary.Model;
using System;

namespace CartShop.Control.Command.ApplicationCommand.ShopProcess
{
    public class Add: ShoppingCommand, IEquatable<Add>
    {
        public override ShopState Id => ShopState.Add;
        private readonly IDisplay _display;
        private readonly Invoice _invoice;

        public Add(IDisplay display, Invoice invoice)
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _invoice = invoice ?? throw new ArgumentNullException(nameof(invoice));
        }

        public override void Execute()
        {
            _display.Clear();
            _display.PrintLine("Select a product by number\n");
            foreach (var kvp in Market.dictProduct)
                _display.PrintLine($"{kvp.Key,2}. {kvp.Value.Name,-30} - {kvp.Value.Price,10:C}");

            _display.Print("\n> ");
            if (int.TryParse(_display.GetInput(), out int productNumber)
                && Market.dictProduct.ContainsKey(productNumber))
            {
                _display.PrintLine("Enter the quantity:");
                if (int.TryParse(_display.GetInput(), out int quantity)
                    && quantity > 0)
                {
                    var selectedProduct = Market.dictProduct[productNumber];
                    _invoice.Cart.Add(new LineItem
                    {
                        Product = selectedProduct,
                        Quantity = quantity
                    });
                }
                else
                {
                    _display.PrintLine("Invalid quantity. Please try again.");
                }
            }
            else
            {
                _display.PrintLine("Invalid selection. Please try again.");
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Add otherCommand && Equals(otherCommand);
        }

        public bool Equals(Add other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}