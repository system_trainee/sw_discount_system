﻿using CartShop.View;
using System;
using System.Threading;

namespace CartShop.Control.Command.ApplicationCommand.ShopProcess
{
    internal class Back: ShoppingCommand, IEquatable<Back>
    {
        public override ShopState Id => ShopState.Back;
        private readonly IDisplay _display;
        private readonly CancellationTokenSource _cancellationTokenSource;

        public Back(IDisplay display, CancellationTokenSource cancellationTokenSource)
        {
            _display = display
                ?? throw new ArgumentNullException(nameof(display));
            _cancellationTokenSource = cancellationTokenSource
                ?? throw new ArgumentNullException(nameof(cancellationTokenSource));
        }

        public override void Execute()
        {
            _display.Clear();
            _display.PrintLine("Shopping Application\n" +
                "Available Commands:\n" +
                "1. Shop\n" +
                "2. help\n" +
                "3. exit");
            _cancellationTokenSource.Cancel();
        }

        public override bool Equals(object obj)
        {
            return obj is Back otherCommand && Equals(otherCommand);
        }

        public bool Equals(Back other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}