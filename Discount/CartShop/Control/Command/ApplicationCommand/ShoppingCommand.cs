﻿namespace CartShop.Control.Command.ApplicationCommand
{
    public abstract class ShoppingCommand: IShoppingCommand
    {
        public abstract ShopState Id { get; }

        public string Name { get; }

        public abstract void Execute();
    }
}