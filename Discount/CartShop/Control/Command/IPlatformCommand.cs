﻿namespace CartShop.Control.Command
{
    public interface IPlatformCommand
    {
        CommandId Id { get; }
        string Name { get; }

        void Execute();
    }
}