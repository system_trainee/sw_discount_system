﻿namespace CartShop.Control.Command
{
    public abstract class PlatformCommand: IPlatformCommand
    {
        public abstract CommandId Id { get; }

        public string Name { get; }

        public abstract void Execute();
    }
}