﻿namespace CartShop.Control.Command
{
    public enum CommandId
    {
        Shop = 1,
        Help,
        Exit,
        Invalid
    }
}