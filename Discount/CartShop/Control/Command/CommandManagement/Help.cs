﻿using CartShop.View;
using System;

namespace CartShop.Control.Command.CommandManagement
{
    public class Help: PlatformCommand, IEquatable<Help>
    {
        public override CommandId Id => CommandId.Help;
        private readonly IDisplay _display;
        public new string Name => this.GetType().Name;

        public Help(IDisplay display)
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
        }

        public override void Execute()
        {
            _display.Clear();
            _display.PrintLine("Help:\n" +
                                "1. Shop <message> - Open Shop.\n" +
                                "2. help - Displays this help message.\n" +
                                "3. exit - Exits the application.");
            _display.ReadKey();
        }

        public override bool Equals(object obj)
        {
            return obj is Help otherCommand && Equals(otherCommand);
        }

        public bool Equals(Help other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}