﻿using System;
using System.Threading;

namespace CartShop.Control.Command.CommandManagement
{
    public class Exit: PlatformCommand, IEquatable<Exit>
    {
        public override CommandId Id => CommandId.Exit;
        private readonly CancellationTokenSource _cancellationTokenSource;
        public new string Name => this.GetType().Name;

        public Exit(CancellationTokenSource cancellationTokenSource)
        {
            _cancellationTokenSource = cancellationTokenSource ?? throw new ArgumentNullException(nameof(cancellationTokenSource));
        }

        public override void Execute()
        {
            _cancellationTokenSource.Cancel();
        }

        public override bool Equals(object obj)
        {
            return obj is Exit otherCommand && Equals(otherCommand);
        }

        public bool Equals(Exit other)
        {
            return other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}