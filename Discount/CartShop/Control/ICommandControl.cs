﻿using CartShop.Control.Command;
using System.Threading;

namespace CartShop.Control
{
    public interface ICommandControl
    {
        CancellationTokenSource CancellationTokenSource { get; }

        void ExecuteCommand(CommandId commandId);

        CommandId ParseData(string input);
    }
}