﻿using CartShop.Control.Command;
using CartShop.Control.Command.ApplicationCommand;
using CartShop.Control.Command.CommandManagement;
using CartShop.View;
using DiscountLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CartShop.Control
{
    public class CommandControl: ICommandControl
    {
        public CancellationTokenSource CancellationTokenSource => _cancellationTokenSource;
        private readonly List<IPlatformCommand> commandList = new List<IPlatformCommand>();
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly Invoice _invoice;
        private readonly IDisplay _display;

        public CommandControl(List<IPlatformCommand> commandList,
                              CancellationTokenSource cancellationTokenSource,
                              Invoice invoice,
                              IDisplay display)
        {
            this.commandList = commandList ?? throw new ArgumentNullException(nameof(commandList));
            _cancellationTokenSource = cancellationTokenSource ?? throw new ArgumentNullException(nameof(cancellationTokenSource));
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _invoice = invoice ?? throw new ArgumentNullException(nameof(invoice));
            RegisterCommand();
        }

        private void RegisterCommand()
        {
            commandList.Add(new Shop(_invoice, _display));
            commandList.Add(new Help(_display));
            commandList.Add(new Exit(_cancellationTokenSource));
        }

        public CommandId ParseData(string input)
        {
            if (Enum.TryParse(input, out CommandId command))
                return command;
            return CommandId.Invalid;
        }

        public void ExecuteCommand(CommandId commandId)
        {
            var command = commandList.FirstOrDefault(c => c.Id == commandId);
            if (command != null)
            {
                _display.PrintLine(command.Name);
                command.Execute();
            }
        }
    }
}