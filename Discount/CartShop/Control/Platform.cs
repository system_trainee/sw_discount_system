﻿using CartShop.Control.Command;
using CartShop.View;
using System;

namespace CartShop.Control
{
    public class Platform: IPlatform
    {
        private readonly IDisplay _display;
        private readonly ICommandControl _commandControl;

        public Platform(IDisplay display, ICommandControl commandControl)
        {
            _display = display ?? throw new ArgumentNullException(nameof(display));
            _commandControl = commandControl ?? throw new ArgumentNullException(nameof(commandControl));
        }

        public void Run()
        {
            try
            {
                RunApplication();
            }
            catch (Exception ex)
            {
                _display.PrintLine($"An error occurred on Shopping Application: {ex.Message}");
            }
        }

        private void RunApplication()
        {
            while (!_commandControl.CancellationTokenSource.IsCancellationRequested)
            {
                _display.Clear();
                _display.PrintLine("Shopping Application\n" +
                    "Available Commands:\n" +
                    "1. Shop\n" +
                    "2. help\n" +
                    "3. exit");

                _display.Print("> ");
                string input = _display.GetInput();
                if (string.IsNullOrWhiteSpace(input))
                    continue;

                var command = _commandControl.ParseData(input);

                if (command != CommandId.Invalid)
                    _commandControl.ExecuteCommand(command);
                else
                {
                    _display.PrintLine("\nInvalid Commnad");
                    _display.ReadKey();
                }
            }
        }
    }
}