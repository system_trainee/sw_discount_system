﻿using DiscountLibrary.Key;
using DiscountLibrary.Model;
using NUnit.Framework;
using NUnit.Framework.Legacy;
using ProductService;

namespace DiscountLibraryTest.RuleTest
{
    [TestFixture]
    public class PromotionTest
    {
        private Invoice? _invoice;
        private ICart? _cart;

        private static List<ISaleRule> rules = new List<ISaleRule>
        {
            new SaleRule(new Promotion(new PromotionRiceBallKeywords(),
                                       new PromotionMeijiKeywords(),
                                       GetPromotionDiscountFactor()))
        };

        [SetUp]
        public void Setup()
        {
            _cart = new ShoppingCart();
            _invoice = new Invoice(rules, _cart);
        }

        [TestCase(new[] { 1, 1, 5, 5, 5, 5, 5, 5 }, 1500)]
        [TestCase(new[] { 2, 1, 0, 0, 0, 5, 5, 5 }, 500)]
        [TestCase(new[] { 3, 1, 0, 0, 5, 0, 5, 5 }, 1000)]
        [TestCase(new[] { 4, 1, 0, 0, 5, 5, 0, 5 }, 1000)]
        [TestCase(new[] { 5, 1, 0, 0, 5, 5, 0, 5 }, 1000)]
        //
        [TestCase(new[] { 6, 2, 5, 5, 4, 5, 5, 5 }, 1300)]
        [TestCase(new[] { 7, 2, 5, 5, 5, 4, 5, 5 }, 1400)]
        [TestCase(new[] { 8, 2, 5, 5, 5, 5, 4, 5 }, 1400)]
        [TestCase(new[] { 9, 2, 1, 1, 5, 5, 5, 4 }, 1300)]
        //
        [TestCase(new[] { 6, 3, 5, 5, 6, 5, 5, 5 }, 1500)]
        [TestCase(new[] { 7, 3, 5, 5, 5, 6, 5, 5 }, 1500)]
        [TestCase(new[] { 8, 3, 5, 5, 5, 5, 6, 5 }, 1500)]
        [TestCase(new[] { 9, 3, 1, 1, 5, 5, 5, 6 }, 1500)]
        //
        [TestCase(new[] { 10, 4, 5, 5, 6, 6, 5, 5 }, 1500)]
        [TestCase(new[] { 11, 4, 5, 5, 6, 5, 6, 5 }, 1500)]
        [TestCase(new[] { 12, 4, 5, 5, 6, 5, 5, 6 }, 1700)]
        //
        [TestCase(new[] { 11, 5, 5, 5, 5, 6, 6, 5 }, 1600)]
        [TestCase(new[] { 12, 5, 5, 5, 5, 6, 5, 6 }, 1600)]
        //
        [TestCase(new[] { 13, 6, 5, 5, 5, 5, 6, 6 }, 1500)]
        //
        [TestCase(new[] { 15, 0, 0, 0, 6, 4, 4, 6 }, 1600)]
        [TestCase(new[] { 16, 0, 0, 0, 4, 6, 6, 4 }, 1400)]
        [TestCase(new[] { 17, 0, 0, 0, 3, 7, 3, 7 }, 1300)]
        [TestCase(new[] { 18, 0, 0, 0, 7, 3, 7, 3 }, 900)]
        public void TestPromotionDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities);
            _cart?.Add(pickedUpProducts);
            var promotionRule = new Promotion(new PromotionRiceBallKeywords(),
                                              new PromotionMeijiKeywords(),
                                              GetPromotionDiscountFactor());

            // Act
            var discount = promotionRule.GetDiscount(_cart!.UserCart);

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        [TestCase(new[] { 1, 1, 0, 0, 1, 0, 1, 0 }, 0)]
        [TestCase(new[] { 1, 1, 0, 0, 1, 1, 0, 0 }, 0)]
        [TestCase(new[] { 0, 0, 0, 0, 0, 0, 0, 0 }, 0)]
        public void TestPromotionNoDiscount(int[] quantities, decimal expectedDiscount)
        {
            // Arrange
            var pickedUpProducts = GetPickedUpProducts(quantities);
            _cart?.Add(pickedUpProducts);
            var promotionRule = new Promotion(new PromotionRiceBallKeywords(),
                                              new PromotionMeijiKeywords(),
                                              GetPromotionDiscountFactor());

            // Act
            var discount = promotionRule.GetDiscount(_cart!.UserCart);

            // Assert
            ClassicAssert.AreEqual(expectedDiscount, discount);
        }

        private IEnumerable<Product> GetPickedUpProducts(int[] quantities)
        {
            var products = new[]
            {
                new ProductService.ProductService.Cart(ProductService.ProductService.GetCoke(), (uint)quantities[0]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetSparklingWater(), (uint)quantities[1]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread44(), (uint)quantities[2]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetBread97(), (uint)quantities[3]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetTunaRiceBall(), (uint)quantities[4]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetCheeseRiceBall(), (uint)quantities[5]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetMeijiMilk225ML(), (uint)quantities[6]),
                new ProductService.ProductService.Cart(ProductService.ProductService.GetMeijiMilk450ML(), (uint)quantities[7]),
            };
            return ProductService.ProductService.GetProductInPack(products);
        }

        internal static Dictionary<(decimal SandwichPrice, decimal MilkPrice), decimal> GetPromotionDiscountFactor()
        {
            return new Dictionary<(decimal SandwichPrice, decimal MilkPrice), decimal>
            {
                {(200m, 200m), 300m},  // Sandwich $200 + Milk $200 costs $300
                {(200m, 300m), 400m},  // Sandwich $200 + Milk $300 costs $400
                {(300m, 300m), 400m}   // Sandwich $300 + Milk $300 costs $400
            };
        }
    }
}