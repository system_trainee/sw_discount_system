﻿namespace ProductService
{
    public class Product
    {
        public Product(string sku)
        {
            this.Sku = sku;
        }

        public string Name { get; internal set; }
        public decimal Price { get; internal set; }
        public string Sku { get; }

        internal Product SetName(string name)
        {
            this.Name = name;
            return this;
        }

        internal Product SetPrice(decimal price)
        {
            this.Price = price;
            return this;
        }
    }
}