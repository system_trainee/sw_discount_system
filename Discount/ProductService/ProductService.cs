﻿using System.Collections.Generic;
using System.Linq;

namespace ProductService
{
    public static class ProductService
    {
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Product> GeSixBeverageInPacks()
        {
            return GetProductInPack(
                new[] {
                    new Cart(ProductService.GetSparklingWater(), 2),
                    new Cart(ProductService.GetCoke(), 4)
                });
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Product> GetFiveSparklingWatersInPacks()
        {
            return GetProductInPack(new[] { new Cart(ProductService.GetSparklingWater(), 5) });
        }

        /// <summary>
        /// </summary>
        /// <param name="productsInCart"></param>
        /// <returns></returns>
        public static IEnumerable<Product> GetProductInPack(IEnumerable<Cart> productsInCart)
        {
            var requests = productsInCart.Where(r => r.Count > 0).ToArray();

            while (requests.Sum(r => r.Count) > 0)
            {
                foreach (var c in requests)
                {
                    yield return new Product(c.ProductInCart.Sku).SetPrice(c.ProductInCart.Price).SetName(c.ProductInCart.Name);
                }
                requests = requests.Select(r => new Cart(r.ProductInCart, r.Count - 1)).Where(r => r.Count > 0).ToArray();
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Product> GetSixSparklingWatersInPacks()
        {
            return GetProductInPack(new[] { new Cart(ProductService.GetSparklingWater(), 6) });
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Product> GetThreeSparklingWaterInPacks()
        {
            return GetProductInPack(new[] { new Cart(ProductService.GetSparklingWater(), 3) });
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Product> GetTwoSparklingWaterInPack()
        {
            return GetProductInPack(new[] { new Cart(ProductService.GetSparklingWater(), 2) });
        }

        public class Cart
        {
            public Cart(Product product, uint count)
            {
                this.ProductInCart = product;
                this.Count = count;
            }

            public uint Count { get; }
            public Product ProductInCart { get; }
        }

        #region Single Beverage

        public static Product GetCoke()
        {
            return new Product("HAPPY-00101").SetPrice(100).SetName("CokeCola");
        }

        public static Product GetLactasoyMilk225ML()
        {
            return new Product("MILK-00101").SetPrice(200).SetName("Lactasoy Milk 225ML");
        }

        public static Product GetLactasoyMilk450ML()
        {
            return new Product("MILK-00102").SetPrice(300).SetName("Lactasoy Milk 450ML");
        }

        public static Product GetMeijiMilk225ML()
        {
            return new Product("MILK-00103").SetPrice(200).SetName("Meiji 225ML");
        }

        public static Product GetMeijiMilk450ML()
        {
            return new Product("MILK-00104").SetPrice(300).SetName("Meiji 450ML");
        }

        public static Product GetSparklingWater()
        {
            return new Product("FMCP-00102").SetPrice(100).SetName("Gerolsteiner Sparkling Water");
        }

        #endregion Single Beverage

        #region Single Food

        public static Product GetCheeseRiceBall()
        {
            return new Product("RB-00101").SetPrice(200).SetName("Chesse Sandwich");
        }

        // 300 + 300 = 600 * 0.5 = 300 // wrong
        public static Product GetBread97()
        {
            return new Product("BD-00102").SetPrice(300).SetName("Bread 97");
        }

        public static Product GetBread44()
        {
            return new Product("BD-00101").SetPrice(150).SetName("Bread 44");
        }

        public static Product GetTunaRiceBall()
        {
            return new Product("RB-00102").SetPrice(300).SetName("Tuna Sandwich");
        }

        #endregion Single Food
    }
}