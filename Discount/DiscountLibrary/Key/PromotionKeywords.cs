﻿using System.Collections.Generic;
using static ProductService.ProductService;

namespace DiscountLibrary.Key
{
    public class PromotionRiceBallKeywords: IKeyword
    {
        public IEnumerable<string> GetNameKeywords()
            => new[] { GetCheeseRiceBall().Name, GetTunaRiceBall().Name };
    }

    public class PromotionMeijiKeywords: IKeyword
    {
        public IEnumerable<string> GetNameKeywords()
            => new[] { GetMeijiMilk225ML().Name, GetMeijiMilk450ML().Name };
    }
}