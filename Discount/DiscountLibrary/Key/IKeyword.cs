﻿using System.Collections.Generic;

namespace DiscountLibrary.Key
{
    public interface IKeyword
    {
        IEnumerable<string> GetNameKeywords();
    }
}