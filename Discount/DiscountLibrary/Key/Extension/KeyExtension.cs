﻿using DiscountLibrary.Model;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.Key.Extension
{
    public static class KeyExtension
    {
        public static IEnumerable<decimal> GetKeyPrices(this IEnumerable<LineItem> products, IEnumerable<string> keywordList)
        {
            return products.Where(product => keywordList.Any(keyword => product.Name.Contains(keyword)))
                           .SelectMany(item => Enumerable.Repeat(item.Price, item.Quantity));
        }

        public static IEnumerable<LineItem> GetKeyProducts(this IEnumerable<LineItem> products, IEnumerable<string> keywordList)
        {
            return products.Where(p => keywordList.Any(keyword => p.Name.Contains(keyword)));
        }
    }
}