﻿using System.Collections.Generic;
using static ProductService.ProductService;

namespace DiscountLibrary.Key
{
    public class BreadKeywords: IKeyword
    {
        public IEnumerable<string> GetNameKeywords()
            => new[] { GetBread44().Name, GetBread97().Name };
    }
}