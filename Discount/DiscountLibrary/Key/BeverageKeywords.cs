﻿using System.Collections.Generic;
using static ProductService.ProductService;

namespace DiscountLibrary.Key
{
    public class BeverageKeywords: IKeyword
    {
        public IEnumerable<string> GetNameKeywords()
            => new[] { GetCoke().Name, GetLactasoyMilk225ML().Name, GetLactasoyMilk450ML().Name, GetSparklingWater().Name };
    }
}