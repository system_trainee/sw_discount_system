﻿using DiscountLibrary.Key;
using DiscountLibrary.Key.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.Model
{
    public class Promotion: ISaleRule
    {
        public decimal DiscountAmount => _discountAmount;
        public string Name => this.GetType().Name;
        public IReadOnlyCollection<Description> Description => _descriptions;
        private List<Description> _descriptions;

        private decimal _discountAmount;
        private readonly IEnumerable<string> _meijiKeywordList;
        private readonly IEnumerable<string> _sandwichKeywordList;
        private readonly Dictionary<(decimal sandwichPrice, decimal milkPrice), decimal> _discountFactorList;

        public Promotion(IKeyword sandwichKeyword,
                         IKeyword meijiKeyword,
                         Dictionary<(decimal sandwichPrice, decimal milkPrice), decimal> discountRules)
        {
            _descriptions = new List<Description>();
            _meijiKeywordList = meijiKeyword.GetNameKeywords() ?? throw new ArgumentNullException(nameof(meijiKeyword));
            _sandwichKeywordList = sandwichKeyword.GetNameKeywords() ?? throw new ArgumentNullException(nameof(sandwichKeyword));
            _discountFactorList = discountRules ?? throw new ArgumentNullException(nameof(discountRules));
        }

        public decimal GetDiscount(IEnumerable<LineItem> products)
        {
            _descriptions.Clear();
            var milks = products.GetKeyPrices(_meijiKeywordList);
            var sandwiches = products.GetKeyPrices(_sandwichKeywordList);
            var filterSandwiches = sandwiches
                .Skip(sandwiches
                    .Where(x => x == sandwiches.Max()).Count() > milks.Where(x => x == sandwiches.Max()).Count() ? 1 : 0);
            var pair = filterSandwiches
                   .OrderByDescending(price => price)
                   .Zip(milks.OrderByDescending(price => price),
                        (sandwich, meiji) => new { sandwich, meiji, discount = CalculateDiscount(sandwich, meiji) });
            var result = pair.GroupBy(p => new { p.sandwich, p.meiji, p.discount })
                      .Select(g => new Description
                      {
                          Name = $"Sandwich: {g.Key.sandwich} and Meiji: {g.Key.meiji}",
                          Quantity = g.Count(),
                          Amount = g.Key.discount * g.Count()
                      });
            _descriptions.AddRange(result);
            return _discountAmount = _descriptions.Sum(x => x.Amount);
        }

        private decimal CalculateDiscount(decimal sandwichPrice, decimal milkPrice)
        {
            return _discountFactorList.TryGetValue((sandwichPrice, milkPrice), out decimal discount)
                ? sandwichPrice + milkPrice - discount
                : 0;
        }
    }
}