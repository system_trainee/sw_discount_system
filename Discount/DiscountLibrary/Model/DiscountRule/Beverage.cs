﻿using DiscountLibrary.Key;
using DiscountLibrary.Key.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.Model
{
    public class Beverage: ISaleRule
    {
        public decimal DiscountAmount => _discountAmount;
        public string Name => this.GetType().Name;
        IReadOnlyCollection<Description> ISaleRule.Description => _descriptions;
        private readonly List<Description> _descriptions;

        private decimal _discountAmount;
        private readonly IEnumerable<string> _keywordList;
        private readonly decimal _discountFactor10Percent = 0.1m;
        private readonly decimal _discountFactor20Percent = 0.2m;

        public Beverage(IKeyword keyword)
            : this(keyword, 0.1m, 0.2m)
        {
            _descriptions = new List<Description>();
        }

        public Beverage(IKeyword keyword, decimal discountFactor10Percent, decimal discountFactor20Percent)
        {
            _keywordList = keyword.GetNameKeywords() ?? throw new ArgumentNullException(nameof(keyword));
            _discountFactor10Percent = discountFactor10Percent > 0 && discountFactor10Percent <= 1
                ? discountFactor10Percent
                : throw new ArgumentException("Discount factor should be between 0 and 1.", nameof(discountFactor10Percent));
            _discountFactor20Percent = discountFactor20Percent > 0 && discountFactor20Percent <= 1
                ? discountFactor20Percent
                : throw new ArgumentException("Discount factor should be between 0 and 1.", nameof(discountFactor20Percent));
        }

        public decimal GetDiscount(IEnumerable<LineItem> products)
        {
            _descriptions.Clear();
            var descriptions = products
                .GetKeyProducts(_keywordList)
                .SelectMany(product => Enumerable.Repeat(product.Price, product.Quantity)
                                            .Select(price => new { product.Name, price }))
                .GroupBy(product => product.Name)
                .SelectMany(beverage =>
                    Enumerable.Range(0, beverage.Count() / 3).Select(perform => new
                    {
                        Name = $"{beverage.Key} {_discountFactor20Percent * 100}% off",
                        Amount = (decimal)3 * beverage.FirstOrDefault().price * _discountFactor20Percent
                    })
                    .Concat(Enumerable.Range(0, beverage.Count() % 3 / 2).Select(i => new
                    {
                        Name = $"{beverage.Key} {_discountFactor10Percent * 100}% off",
                        Amount = (decimal)2 * beverage.FirstOrDefault().price * _discountFactor10Percent
                    }))
                )
                .GroupBy(product => product.Name)
                .Select(product => new Description
                {
                    Name = product.Key,
                    Quantity = product.Count(),
                    Amount = product.Sum(d => d.Amount)
                });
            _descriptions.AddRange(descriptions);
            return _discountAmount = _descriptions.Sum(x => x.Amount);
        }
    }
}