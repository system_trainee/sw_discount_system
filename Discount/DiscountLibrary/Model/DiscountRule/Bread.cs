﻿using DiscountLibrary.Key;
using DiscountLibrary.Key.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.Model
{
    public class Bread: ISaleRule
    {
        public decimal DiscountAmount => _discountAmount;
        public string Name => this.GetType().Name;
        public IReadOnlyCollection<Description> Description => _descriptions;
        private List<Description> _descriptions;

        private decimal _discountAmount;
        private readonly IEnumerable<string> _keywordList;
        private decimal _discountFactor = 0.5m;

        public Bread(IKeyword keyword)
            : this(keyword, 0.5m)
        {
            _descriptions = new List<Description>();
        }

        public Bread(IKeyword keyword, decimal discountFactor)
        {
            _keywordList = keyword.GetNameKeywords() ?? throw new ArgumentNullException(nameof(keyword));
            _discountFactor = discountFactor > 0 && discountFactor <= 1
                ? discountFactor
                : throw new ArgumentException("Discount factor should be between 0 and 1.", nameof(discountFactor));
        }

        public decimal GetDiscount(IEnumerable<LineItem> products)
        {
            var keyProduct = products.GetKeyPrices(_keywordList);
            var productCount = keyProduct.Count();
            _descriptions.Clear();
            _descriptions.AddRange(keyProduct
                .OrderBy(price => price)
                .Take(productCount % 2 == 0
                    ? productCount / 2
                    : ( productCount - 1 ) / 2)
                .GroupBy(p => p)
                .Select(g => new Description
                {
                    Name = $"{this.Name} {_discountFactor * 100}% off",
                    Quantity = g.Count(),
                    Amount = g.Key * _discountFactor * g.Count(),
                }));
            return _discountAmount = _descriptions.Sum(s => s.Amount);
        }
    }
}