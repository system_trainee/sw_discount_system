﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.Model
{
    public class Rebate: ISaleRule
    {
        public decimal DiscountAmount => _discountAmount;
        public string Name => this.GetType().Name;
        public IReadOnlyCollection<Description> Description => _descriptions;
        private List<Description> _descriptions;

        private decimal _discountAmount;
        private readonly decimal _rebateAmount;
        private readonly decimal _purchaseThreshold;

        public Rebate()
        {
            _descriptions = new List<Description>();
        }

        public Rebate(decimal purchaseThreshold, decimal rebateAmount)
            : this()
        {
            if (purchaseThreshold <= 0)
                throw new ArgumentException("Purchase threshold must be greater than zero.", nameof(purchaseThreshold));
            if (rebateAmount <= 0)
                throw new ArgumentException("Rebate amount must be greater than zero.", nameof(rebateAmount));

            _purchaseThreshold = purchaseThreshold;
            _rebateAmount = rebateAmount;
        }

        public decimal GetDiscount(IEnumerable<LineItem> products)
        {
            _descriptions.Clear();
            _descriptions.AddRange(Enumerable.Empty<Description>());
            return products.Sum(product => product.Price * product.Quantity) >= _purchaseThreshold
                ? _discountAmount = _rebateAmount
                : _discountAmount = 0; ;
        }
    }
}