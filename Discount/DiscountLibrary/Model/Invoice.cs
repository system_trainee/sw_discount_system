﻿using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.Model
{
    public class Invoice
    {
        public ICart Cart;
        public decimal TotalAmount => _totalAmount;
        public decimal TotalDiscount => _totalDiscount;

        private readonly IEnumerable<ISaleRule> _rules;
        private decimal _totalAmount => CalculateTotal();
        private decimal _totalDiscount => CalculateDiscount();

        public Invoice()
        {
        }

        public Invoice(IEnumerable<ISaleRule> rules, ICart shoppingCart)
            : this()
        {
            _rules = rules;
            Cart = shoppingCart;
        }

        public IEnumerable<ISaleRule> GetSaleRuleInformation()
        {
            foreach (var rule in _rules)
                yield return rule;
        }

        private decimal CalculateTotal()
        {
            return Cart.UserCart.Sum(product => product.Price * product.Quantity);
        }

        private decimal CalculateDiscount()
        {
            return _rules.Sum(rule => rule.GetDiscount(Cart.UserCart));
        }
    }
}