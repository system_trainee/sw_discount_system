﻿using ProductService;

namespace DiscountLibrary.Model
{
    public class LineItem
    {
        private string _name;
        private decimal _price;
        public Product Product { get; set; }
        public string Sku => Product.Sku;

        public string Name
        {
            get => _name ?? Product?.Name;
            set => _name = value;
        }

        public decimal Price
        {
            get => _price != 0 ? _price : ( Product?.Price ?? 0 );
            set => _price = value;
        }

        public int Quantity { get; set; }
    }
}