﻿using System.Collections.Generic;

namespace DiscountLibrary.Model
{
    public class SaleRule: ISaleRule
    {
        protected ISaleRule _regulation;

        public SaleRule(ISaleRule regulation)
        {
            _regulation = regulation;
        }

        public decimal DiscountAmount => _regulation.DiscountAmount;

        public string Name => _regulation.Name;

        public IReadOnlyCollection<Description> Description => _regulation.Description;

        public decimal GetDiscount(IEnumerable<LineItem> products)
        {
            return _regulation.GetDiscount(products);
        }
    }
}