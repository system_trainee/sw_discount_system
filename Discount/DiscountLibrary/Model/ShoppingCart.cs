﻿using ProductService;
using System.Collections.Generic;
using System.Linq;

namespace DiscountLibrary.Model
{
    public class ShoppingCart: ICart
    {
        public IEnumerable<LineItem> UserCart => _userCart.Values;
        private Dictionary<int, LineItem> _userCart;
        private int _nextId;

        public ShoppingCart()
        {
            _userCart = new Dictionary<int, LineItem>();
            _nextId = 1;
        }

        public void Add(IEnumerable<Product> products)
        {
            foreach (var product in products)
            {
                var existingItem = _userCart.Values.FirstOrDefault(item => item.Sku == product.Sku);

                if (existingItem != null)
                    existingItem.Quantity++;
                else
                    _userCart.Add(_nextId++, new LineItem { Product = product, Quantity = 1 });
            }
        }

        public void Add(params Product[] products)
        {
            foreach (var product in products)
            {
                var existingItem = _userCart.Values.FirstOrDefault(item => item.Sku == product.Sku);

                if (existingItem != null)
                    existingItem.Quantity++;
                else
                    _userCart.Add(_nextId++, new LineItem { Product = product, Quantity = 1 });
            }
        }

        public void Add(LineItem lineItem)
        {
            var existingItem = _userCart.Values.FirstOrDefault(item => item.Sku == lineItem.Sku);

            if (existingItem != null)
                existingItem.Quantity += lineItem.Quantity;
            else
                _userCart.Add(_nextId++, lineItem);
        }

        public void Add(IEnumerable<LineItem> lineItems)
        {
            foreach (var lineItem in lineItems)
                Add(lineItem);
        }

        public void Edit(LineItem lineItem, int newQuantity)
        {
            var itemToEdit = _userCart.Values.FirstOrDefault(item => item.Sku == lineItem.Sku);
            if (itemToEdit != null)
                itemToEdit.Quantity = newQuantity;
        }

        public void Remove(LineItem lineItem)
        {
            var keyToRemove = _userCart.FirstOrDefault(kvp => kvp.Value.Sku == lineItem.Sku).Key;
            if (keyToRemove != 0)
                _userCart.Remove(keyToRemove);
        }

        public void Clear()
        {
            _userCart.Clear();
        }
    }
}