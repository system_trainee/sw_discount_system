﻿namespace DiscountLibrary.Model
{
    public class Description
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
    }
}