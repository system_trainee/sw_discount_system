﻿using System.Collections.Generic;

namespace DiscountLibrary.Model
{
    public interface ISaleRule
    {
        string Name { get; }
        decimal DiscountAmount { get; }

        decimal GetDiscount(IEnumerable<LineItem> products);

        IReadOnlyCollection<Description> Description { get; }
    }
}