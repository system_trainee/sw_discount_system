﻿using ProductService;
using System.Collections.Generic;

namespace DiscountLibrary.Model
{
    public interface ICart
    {
        IEnumerable<LineItem> UserCart { get; }

        void Add(IEnumerable<Product> products);

        void Add(params Product[] products);

        void Add(LineItem lineItem);

        void Add(IEnumerable<LineItem> lineItems);

        void Edit(LineItem lineItem, int newQuantity);

        void Remove(LineItem lineItem);

        void Clear();
    }
}